# convert to exe file by using the command: $pyinstaller main.py -F

import random, time, ConfigParser, simplejson as sjson, csv, sys
from datetime import timedelta, date, datetime

err_counter = 0
restrictions_per_intern = {}
max_num_of_shifts_per_intern = {}
min_num_of_shifts_per_intern = {}
max_num_of_weekend_shifts_per_intern = {}
min_num_of_weekend_shifts_per_intern = {}
start_day = ""
end_day = ""
days = []
weekend_days = []
too_close_shifts = 0
departments_only = []
build_in_shifts = {}
max_number_of_close_shifts = 0


def is_stuck_checker(counter, function_name):
    max_number_of_iterations = 5000
    if counter['num'] >= max_number_of_iterations:
        print function_name, " is stuck"
        sys.exit(1)
    counter['num'] += 1


def report_schedule(placement, stats_per_intern):
    sorted_placement = sorted([(k, v) for k, v in placement.items()], key=sorting)
    string = str(sorted_placement).replace('),', '),\n')
    string += "\n\n" + "*" * 40 + "\n\n"
    string += str(stats_per_intern).replace("},", "},\n")
    string += "\n\ntoo close shifts: " + str(too_close_shifts)
    print string
    write_to_file(placement, stats_per_intern)


def convert_string_to_date(string_date):
    [year, month, day] = string_date.split('-')
    return date(int(year), int(month), int(day))


def sorting(spec_date):
    date_elements = spec_date[0].split('-')
    return date_elements[0], date_elements[1], date_elements[2]


def get_list_of_days(start_date, end_date):
    list_of_days = []

    def get_date_range(start_d, end_d):
        for n in range(int((end_d - start_d).days)):
            yield start_date + timedelta(n)

    for single_date in get_date_range(start_date, end_date):
        list_of_days.append(single_date.strftime("%Y-%m-%d"))
    list_of_days.append(end_date.strftime("%Y-%m-%d"))
    return list_of_days


def get_the_day_before(cur_date, num_days_before):
    date_string = datetime.strptime(cur_date, "%Y-%m-%d")
    date_of_day_before = date_string + timedelta(days=int(num_days_before) * (-1))
    return datetime.strftime(date_of_day_before, "%Y-%m-%d")


def check_justice(stats_for_every_intern):
    global min_num_of_weekend_shifts_per_intern, min_num_of_shifts_per_intern
    for intern in stats_for_every_intern.keys():
        if intern in min_num_of_shifts_per_intern.keys() and \
                        stats_for_every_intern[intern]['num_of_shifts'] < min_num_of_shifts_per_intern[intern]:
            return False
    for intern in stats_for_every_intern.keys():
        if intern in min_num_of_weekend_shifts_per_intern.keys() and \
                        stats_for_every_intern[intern]['num_of_weekends'] < min_num_of_weekend_shifts_per_intern[
                    intern]:
            return False
    return True


def pick_interns_per_shift(shifts_left_for_interns, placement, cur_day, num_of_interns_in_shift,
                           weekend_shifts_per_intern):
    global restrictions_per_intern, days, too_close_shifts, departments_only, start_day
    prev_day_interns = placement[get_the_day_before(cur_day, 1)] \
        if get_the_day_before(cur_day, 1) >= start_day else []
    prev_prev_day_interns = placement[get_the_day_before(cur_day, 2)] \
        if get_the_day_before(cur_day, 2) >= start_day else []
    counter = 3
    while True:
        available_interns = filter(lambda intern: shifts_left_for_interns[intern] > 0 and
                                    intern not in prev_day_interns and cur_day not in restrictions_per_intern[intern],
                                    shifts_left_for_interns)
        if counter > 0:
            # we first try to pick interns who haven't been placed for shifts two days ago
            available_interns = filter(lambda intern: intern not in prev_prev_day_interns, available_interns)
            counter -= 1
        if cur_day in weekend_days:
            available_interns = filter(lambda intern: (intern in weekend_shifts_per_intern.keys() and
                                                       weekend_shifts_per_intern[intern] > 0), available_interns)
        if len(available_interns) < 2:
            return []
        if len(filter(lambda intern: intern not in departments_only, available_interns)) == 0:
            return []
        selected_interns = random.sample(available_interns, num_of_interns_in_shift)
        prev_prev_day_interns = placement[get_the_day_before(cur_day, 2)] if get_the_day_before(
            cur_day, 2) in days else []
        for selected_intern in selected_interns:
            if selected_intern in prev_prev_day_interns:
                too_close_shifts += 1
        if len(filter(lambda intern: intern not in departments_only, selected_interns)) == 0:
            continue
        for intern in selected_interns:
            shifts_left_for_interns[intern] -= 1
            if cur_day in weekend_days:
                weekend_shifts_per_intern[intern] -= 1
        return selected_interns


def fill_interns_in_schedule(shifts_left_for_interns, weekend_left_per_intern, placement):
    global days, err_counter, too_close_shifts, build_in_shifts
    num_of_interns_to_place = 2
    flag = 0
    for day in days:
        selected_interns = []
        if day in build_in_shifts.keys():
            selected_interns.extend(build_in_shifts[day])
            interns_to_place = pick_interns_per_shift(shifts_left_for_interns, placement, day,
                                                      num_of_interns_to_place - len(build_in_shifts[day]),
                                                      weekend_left_per_intern)
        else:
            interns_to_place = pick_interns_per_shift(shifts_left_for_interns, placement, day, num_of_interns_to_place,
                                                      weekend_left_per_intern)
        selected_interns.extend(interns_to_place)
        if len(selected_interns) < num_of_interns_to_place:
            if err_counter % 50 == 0:
                print "placement failed: " + str(err_counter)
            err_counter += 1
            flag = 1
            break
        placement[day] = selected_interns
    if flag:
        return None
    return placement


def write_to_file(placement, stats_per_intern):
    filename = str(int(time.time() % 10000)) + ".csv"
    print "file name: ", filename
    with open(filename, 'wb') as filename:
        writer = csv.writer(filename, dialect='excel')
        for day in days:
            writer.writerow([day, placement[day][0], placement[day][1]])
        writer.writerow([''])
        writer.writerow([''])
        writer.writerow(['intern', 'num_of_shifts', 'num_of_weekends', 'num_of_close_shifts'])
        for intern in stats_per_intern.keys():
            writer.writerow(
                [intern, stats_per_intern[intern]['num_of_shifts'], stats_per_intern[intern]['num_of_weekends'],
                 stats_per_intern[intern]['num_of_close_shifts']])


def get_intern_stats(placement):
    global restrictions_per_intern, weekend_days
    interns_in_built_in_shifts = []
    map(lambda intern: interns_in_built_in_shifts.extend(intern), build_in_shifts.values())
    list_of_interns = set(restrictions_per_intern.keys()) | set(interns_in_built_in_shifts)
    intern_stat = {intern: {'num_of_shifts': 0, 'num_of_weekends': 0, 'num_of_close_shifts': 0}
                   for intern in list_of_interns}
    for day in placement:
        for intern in placement[day]:
            intern_stat[intern]['num_of_shifts'] += 1
            if day in weekend_days:
                intern_stat[intern]['num_of_weekends'] += 1
            if get_the_day_before(day, 2) > start_day and intern in placement[get_the_day_before(day, 2)]:
                intern_stat[intern]['num_of_close_shifts'] += 1
    return intern_stat


def read_config_file():
    global start_day, end_day, restrictions_per_intern, max_num_of_shifts_per_intern, weekend_days, departments_only, \
        build_in_shifts, min_num_of_shifts_per_intern, max_number_of_close_shifts, max_num_of_weekend_shifts_per_intern, min_num_of_weekend_shifts_per_intern
    config = ConfigParser.ConfigParser()
    config.read("config.txt")
    start_day = config.get("section", "start_day").replace("\"", "")
    end_day = config.get("section", "end_day").replace("\"", "")
    max_number_of_close_shifts = int(config.get("section", "max_number_of_close_shifts"))
    weekend_days = sjson.loads(config.get("section", "weekend_days").replace("\n", "").replace("\'", "\""))
    restrictions_per_intern = sjson.loads(config.get("section", "restrictions_per_intern").replace("\n", "")
                                          .replace("\'", "\""))
    departments_only = sjson.loads(config.get("section", "departments_only").replace("\n", "").replace("\'", "\""))
    max_num_of_shifts_per_intern = sjson.loads(config.get("section", "max_num_of_shifts_per_intern").replace("\n", "")
                                               .replace("\'", "\""))
    max_num_of_weekend_shifts_per_intern = sjson.loads(config.get("section", "max_num_of_weekend_shifts_per_intern")
                                                       .replace("\n", "").replace("\'", "\""))
    min_num_of_shifts_per_intern = sjson.loads(config.get("section", "min_num_of_shifts_per_intern").replace("\n", "")
                                               .replace("\'", "\""))
    min_num_of_weekend_shifts_per_intern = sjson.loads(config.get("section", "min_num_of_weekend_shifts_per_intern")
                                                       .replace("\n", "").replace("\'", "\""))
    build_in_shifts = sjson.loads(config.get("section", "built_in_shifts").replace("\n", "").replace("\'", "\""))


def main():
    global days, start_day, end_day, max_num_of_shifts_per_intern, too_close_shifts
    read_config_file()
    days = get_list_of_days(convert_string_to_date(start_day), convert_string_to_date(end_day))
    while True:
        counter = {'num': 0}
        is_stuck_checker(counter, "main")
        shifts_left_for_interns = max_num_of_shifts_per_intern.copy()
        weekend_left_for_interns = max_num_of_weekend_shifts_per_intern.copy()
        placement = {value: [] for value in days}
        too_close_shifts = 0
        ruffle_res = fill_interns_in_schedule(shifts_left_for_interns, weekend_left_for_interns, placement)
        if ruffle_res is not None and too_close_shifts <= max_number_of_close_shifts:
            stats_per_intern = get_intern_stats(placement)
            if not check_justice(stats_per_intern):
                continue
            report_schedule(placement, stats_per_intern)
            break
    raw_input("Press enter to exit ;)")


if __name__ == '__main__':
    main()
